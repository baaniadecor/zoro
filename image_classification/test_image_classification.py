import json
import boto3
import requests

#########################
# DECORCOLOR DOMAIN
#########################
DEFAULT_THR_DECORCOLOR = 0.0
SCORE_SCALE_DECORCOLOR = 1000
TAG_DECORCOLOR_PARAMS = {
    1: {"ful": 'black', "abbv": "bk"},
    2: {"ful": 'blue', "abbv": "bu"},
    3: {"ful": 'brown', "abbv": "br"},
    4: {"ful": 'gray', "abbv": "gy"},
    5: {"ful": 'green', "abbv": "gn"},
    6: {"ful": 'orange', "abbv": "or"},
    7: {"ful": 'pink', "abbv": "pk"},
    8: {"ful": 'purple', "abbv": "pr"},
    9: {"ful": 'red', "abbv": "rd"},
    10: {"ful": 'tan', "abbv": "tn"},
    11: {"ful": 'white', "abbv": "wh"},
    12: {"ful": 'yellow', "abbv": "yw"}
}

#########################
# DECORSTYLE DOMAIN
#########################
DEFAULT_THR_DECORSTYLE = 0.0
SCORE_SCALE_DECORSTYLE = 1000
TAG_DECORSTYLE_PARAMS = {
    1: {"ful": 'art_deco', "abbv": "ad"},
    2: {"ful": 'asian', "abbv": "as"},
    3: {"ful": 'contemporary', "abbv": "ctp"},
    4: {"ful": 'cottage', "abbv": "cot"},
    5: {"ful": 'country', "abbv": "cnt"},
    6: {"ful": 'craftsman', "abbv": "crf"},
    7: {"ful": 'eclectic', "abbv": "ec"},
    8: {"ful": 'mediterranean', "abbv": "med"},
    9: {"ful": 'modern', "abbv": "mod"},
    10: {"ful": 'rustic', "abbv": "rus"},
    11: {"ful": 'traditional', "abbv": "tra"},
    12: {"ful": 'tropical', "abbv": "tro"}
}

#########################
# SUBSCENE TYPE DOMAIN
#########################
DEFAULT_THR_SUBSCENE = 0.0
SCORE_SCALE_SUBSCENE = 1000
TAG_SUBSCENE_PARAMS = {
    1: {"ful": 'a_3_4_bathrooms', "abbv": "bt34"},
    2: {"ful": 'full_bathrooms', "abbv": "btfl"},
    3: {"ful": 'guest_bedrooms', "abbv": "bdgt"},
    4: {"ful": 'kids_bathrooms', "abbv": "btk"},
    5: {"ful": 'kids_bedrooms', "abbv": "bdk"},
    6: {"ful": 'master_bathrooms', "abbv": "btm"},
    7: {"ful": 'master_bedrooms', "abbv": "bdm"}
}


scene_to_subscene_mapper = {
    24: [3,5,7],
    26: [1,2,4,6]
}

#########################
# SCENE TYPE DOMAIN
#########################
DEFAULT_THR_SCENE = 0.0
SCORE_SCALE_SCENE = 1000
TAG_SCENE_PARAMS = {
    1: {"ful": "unit_dining_room", "abbv": "udnr"},
    2: {"ful": "common_deck", "thr": 0.4500, "abbv": "cdck"},
    3: {"ful": "unit_deck", "abbv": "udck"},
    4: {"ful": "common_game_room", "thr": 0.8443, "abbv": "cgm"},
    5: {"ful": "common_sauna", "abbv": "csa"},
    6: {"ful": "common_exterior", "thr": 0.4598, "abbv": "cxt"},
    7: {"ful": "unit_exterior", "abbv": "uxt"},
    8: {"ful": "common_sign", "abbv": "csgn"},
    9: {"ful": "common_indoor_others", "abbv": "cio"},
    10: {"ful": "unit_indoor_others", "abbv": "uio"},
    11: {"ful": "unit_living_room", "abbv": "ulvr"},
    12: {"ful": "unit_kitchen", "abbv": "uktc"},
    13: {"ful": "unit_laundry", "abbv": "ula"},
    14: {"ful": "common_fitness", "abbv": "cftn"},
    15: {"ful": "unit_walk_in_closet", "abbv": "uwic"},
    16: {"ful": "common_lobby", "thr": 0.5961, "abbv": "clby"},
    17: {"ful": "common_rooftop", "thr": 0.7234, "abbv": "crft"},
    18: {"ful": "unit_rooftop", "abbv": "urft"},
    19: {"ful": "common_building_shot", "abbv": "cbld"},
    20: {"ful": "common_theater", "abbv": "ctht"},
    21: {"ful": "common_patio", "thr": 0.6401, "abbv": "cpto"},
    22: {"ful": "unit_patio", "abbv": "upto"},
    23: {"ful": "common_outdoor_area", "thr": 0.5076, "abbv": "coa"},
    24: {"ful": "unit_bedroom", "abbv": "ubd"},
    25: {"ful": "common_parking", "abbv": "cprk"},
    26: {"ful": "unit_bathroom", "abbv": "ubt"},
    27: {"ful": "unit_living_space", "abbv": "ulvs"},
    28: {"ful": "common_swimming_pool", "thr": 0.864, "abbv": "csw"},
    29: {"ful": "common_view", "thr": 0.8913, "abbv": "cvw"},
    30: {"ful": "unit_view", "abbv": "uvw"},
    31: {"ful": "unit_hall_corridor_stairs", "abbv": "uhcs"},
    32: {"ful": "floorplan", "abbv": "flrp" },
    33: {"ful": "deck_patio_rooftop", "abbv": "dpr"},
    34: {"ful": "views", "abbv": "vws"},
    201: {"ful": 'landscape_yard', "abbv": "lsy"},
    202: {"ful": 'home_exteriors', "abbv": "hxt"},
    203: {"ful": 'front_doors', "abbv": "ftd"},
    204: {"ful": 'garages', "abbv": "grg"},
    205: {"ful": 'basements', "abbv": "bsmt"},
    206: {"ful": 'staircases', "abbv": "stcs"},
    207: {"ful": 'hallways', "abbv": "hlwy"},
    208: {"ful": 'entryways', "abbv": "etwy"},
    209: {"ful": 'bars', "abbv": "bars"},
    210: {"ful": 'playrooms', "abbv": "plrm"},
    211: {"ful": 'home_offices', "abbv": "hmof"},
    212: {"ful": 'family_rooms', "abbv": "fmrm"},
    213: {"ful": 'attics', "abbv": "attc"},
    214: {"ful": 'mud_rooms', "abbv": "mdrm"},
    215: {"ful": 'pantries', "abbv": "pntr"},
    216: {"ful": 'wine_cellars', "abbv": "wncl"},
    217: {"ful": 'powder_rooms', "abbv": "pdrm"},
    218: {"ful": 'nurseries', "abbv": "nrs"},
    219: {"ful": 'great_rooms', "abbv": "grrm"},
    220: {"ful": 'libraries', "abbv": "lib"},
    221: {"ful": 'hot_tubs', "abbv": "hott"}
}

def call_lambda_func(lambda_func, func_name, request_dict, is_async = False):
    for i in range(3):
        try:
            response = lambda_func.invoke(
                FunctionName=func_name,
                InvocationType='RequestResponse' if not is_async else 'Event',
                Qualifier='release',
                Payload = json.dumps(request_dict)
            )
            if is_async:
                if response['StatusCode'] == 202:
                    return True
                else:
                    raise Exception('Failed to call ' + func_name + ':' + json.dumps(response))
            reponse_payload = response['Payload'].read()
            try:
                results = json.loads(reponse_payload)
            except:
                raise Exception(json.dumps(reponse_payload))
            if isinstance(results, dict) and ('errorType' in results or 'errorMessage' in results):
                raise Exception(json.dumps(reponse_payload))
            return results
        except:
            if i == 2:
                raise
            #logger.error('Failed to call function ' + func_name + ' for ' + repr(request_dict), exc_info=True)

#########################
# initiation
#########################
req_session = requests.Session()
session = boto3.session.Session(profile_name="bn2018")
lambda_func = session.client('lambda',region_name="ap-southeast-1")

if __name__ == "__main__":
    event = {
        "img": [
            # {"id": "101", "url": "https://ssl.cdn-redfin.com/system_files/media/118873_JPG/item_17.jpg"},
            # {"id": "102", "url": "https://ssl.cdn-redfin.com/system_files/media/118873_JPG/item_18.jpg"},
            # {"id": "103", "url": "https://ssl.cdn-redfin.com/system_files/media/118873_JPG/item_28.jpg"},
            # {"id": "104", "m": "https://ssl.cdn-redfin.com/system_files/media/118873_JPG/item_28.jpg"},
            # {"id": "gym_a1", "url": "https://ssl.cdn-redfin.com/photo/106/bigphoto/532/RX-10407532_12_1.jpg"},
            # {"id": "gym_a2", "url": "https://ssl.cdn-redfin.com/photo/106/bigphoto/327/RX-10429327_36_1.jpg"},
            # {"id": "gym_b1", "url": "https://ssl.cdn-redfin.com/photo/106/bigphoto/907/RX-10414907_30_1.jpg"},
            # {"id": "gym_b2", "url": "https://ssl.cdn-redfin.com/photo/106/bigphoto/073/RX-10422073_51_0.jpg"},
            #{"id": "kidbed1", "url": "https://photos.zillowstatic.com/i_g/ISd8joansrrgyb0000000000.jpg"},
            #{"id": "kidbed2", "url": "https://photos.zillowstatic.com/i_g/IShrcvjugqgitd0000000000.jpg"},
            #{"id": "etwy1", "url": "https://photos.zillowstatic.com/i_g/IS91f5edcrdo760000000000.jpg"},
            {"id": "yrd1", "url": "https://iss.zillowstatic.com/image/tropical-landscape-yard-with-fence-i_g-ISliodw06393w90000000000-Pho9d.jpg"},
            {"id": "bt34", "url": "https://iss.zillowstatic.com/image/traditional-3-4-bathroom-with-frameless-shower-rain-shower-and-built-in-bookshelf-i_g-ISppc87pi2djt61000000000-3cUVo.jpg"},
            {"id": "259938", "url": "https://baania.com/sites/default/files/unittype/12293/photo/259938.jpg"}
        ]
    }

    output = call_lambda_func(lambda_func, 'TagImages', event, is_async=False)
    # print(json.dumps(output, indent=4)) 

    ### display the results
    for res in output['out']:
        image_url = res.get("url")
        scene = res.get("scene","unknown")
        subscene = res.get("subscene","unknown")
        style = res.get("style","unknown")
        color = res.get("color","unknown")
        
        ### validate sub-scene
        subscene_text = TAG_SUBSCENE_PARAMS[subscene]["ful"] if subscene in scene_to_subscene_mapper.get(scene,[]) else '-'
        
        ### display photos
        # display(Image(url=image_url, width=300, unconfined=True))
        caption = '''url: {}\nscene: {}, subscene: {}, style: {}, color: {}
        '''.format(image_url, 
                   TAG_SCENE_PARAMS[scene]["ful"], 
                   subscene_text, 
                   TAG_DECORSTYLE_PARAMS[style]["ful"], 
                   TAG_DECORCOLOR_PARAMS[color]["ful"])
        print(caption)    
